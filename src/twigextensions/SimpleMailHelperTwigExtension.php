<?php
/**
 * Simple Core plugin for Craft CMS 3.x
 *
 * The core library that used by Simple Team
 *
 * @link      https://simple.com.au
 * @copyright Copyright (c) 2019 Simple Integrated Marketing
 */

namespace simple\simplemailhelper\twigextensions;

use craft\helpers\ArrayHelper;
use craft\web\View;
use simple\simplemailhelper\assetbundles\recaptcha\ReCAPTCHAAssets;
use simple\simplemailhelper\SimpleMailHelper;

use Craft;

/**
 * @author    Simple Integrated Marketing
 * @package   SimpleCore
 * @since     0.0.1
 */
class SimpleMailHelperTwigExtension extends \Twig_Extension
{
    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'SimpleCore';
    }

    /**
     * @inheritdoc
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('bug', [$this, 'bug'], ['is_safe' => ['html']]),
        ];
    }

    /**
     * @inheritdoc
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('bug', [$this, 'bug'], ['is_safe' => ['html']]),
        ];
    }

    /**
     * Generate the reCAPTCHA frontend widget
     *
     * @return string
     */
    public function bug($options = []): string
    {
        return '🐞';
    }
}
