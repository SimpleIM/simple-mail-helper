<?php


namespace simple\simplemailhelper\services;

use Craft;
use craft\base\Component;


class SimpleMailHelperService extends Component
{
    public function renderEmailTemplate($params) {

        //  Set Defaults
        $defaults = [
            "showFormMeta" => true,
            "showHtmlFields" => true,
            "showEmptyFields" => false,
            "showAdminLinks" => false,
            "logoPath" => \Craft::$app->assetManager->getPublishedUrl('@simple/simplemailhelper/assetbundles/mailhelper/dist/img/logo.png', true),
            "buttonColor" => "#2d95e3",
            "footerLinks" => [],
            "notificationMessage" => null,
            "title" => "New Form Submission",
        ];

        //  Merge with user params
        $params = array_merge($defaults, $params);

        //  Get the current template mode
        $existingTemplateMode = Craft::$app->view->getTemplateMode();

        //  Set template mode to CP
        Craft::$app->view->setTemplateMode(craft\web\View::TEMPLATE_MODE_CP);

        //  Set Defaults
        $html = Craft::$app->view->renderTemplate('simple-mail-helper/email/notification', [ 'settings' => $params, 'submission' => $params['submission'] ]);

        //  Restore template mode
        Craft::$app->view->setTemplateMode($existingTemplateMode);

        // Return the result
        return $html;

    }

}
