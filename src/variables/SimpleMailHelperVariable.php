<?php
/**
 * Simple Core plugin for Craft CMS 3.x
 *
 * The core library that used by Simple Team
 *
 * @link      https://simple.com.au
 * @copyright Copyright (c) 2019 Simple Integrated Marketing
 */

namespace simple\simplemailhelper\variables;

use simple\simplemailhelper\SimpleMailHelper;

use Craft;

/**
 * @author    Simple Integrated Marketing
 * @package   SimpleCore
 * @since     0.0.1
 */
class SimpleMailHelperVariable
{
    // Public Methods
    // =========================================================================

    /**
     * @return string
     */
    public function renderEmailTemplate($params)
    {
        return SimpleMailHelper::$plugin->simpleMailHelper->renderEmailTemplate($params);
    }
}
