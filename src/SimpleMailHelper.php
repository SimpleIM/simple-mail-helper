<?php
/**
 * Simple Mail Helper plugin for Craft CMS 3.x
 *
 * Provide the toolkit to generate Simple mails.
 *
 * @link      https://simple.com.au
 * @copyright Copyright (c) 2019 Simple Integrated Marketing
 */

namespace simple\simplemailhelper;


use craft\base\Plugin;
use craft\web\twig\variables\CraftVariable;
use simple\simplemailhelper\services\SimpleMailHelperService;
use simple\simplemailhelper\twigextensions\SimpleMailHelperTwigExtension;
use simple\simplemailhelper\variables\SimpleMailHelperVariable;
use yii\base\Event;
use Craft;

/**
 * Class SimpleMailHelper
 *
 * @author    Simple Integrated Marketing
 * @package   SimpleMailHelper
 * @since     0.0.1
 * @property SimpleMailHelperService $simpleMailHelper
 *
 */
class SimpleMailHelper extends Plugin
{
    // Static Properties
    // =========================================================================

    /**
     * @var SimpleMailHelper
     */
    public static $plugin;

    // Public Properties
    // =========================================================================

    /**
     * @var string
     */
    public $schemaVersion = '0.0.1';

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        self::$plugin = $this;

        $this->setComponents([
            'simpleMailHelper' => SimpleMailHelperService::class
        ]);

        Craft::$app->view->registerTwigExtension(new SimpleMailHelperTwigExtension());
        Event::on(
            CraftVariable::class,
            CraftVariable::EVENT_INIT,
            function (Event $event) {
                /** @var CraftVariable $variable */
                $variable = $event->sender;
                $variable->set('simpleMailHelper', SimpleMailHelperVariable::class);
            }
        );
    }

    // Protected Methods
    // =========================================================================

}
